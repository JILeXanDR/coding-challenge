# _enersis_ coding challenge

## Prerequisites
- docker
- node
- pgadmin (optional)

## Setup
- Navigate to the _database_ directory
- Build the image: `docker build -t coding-challenge-db .`
- Run db container: `docker run -d -p 5432:5432 coding-challenge-db`
- Now you can connect to the database at _localhost:5432_ using pgadmin (username: postgres, password: postgres)
- Navigate to the project root
- Install dependencies: `npm install`
- Run the application: `npm run start`

## Test
- `localhost:3000` shows the text _enersis coding challenge_
- `localhost:3000/static/sample.html` serves the file `static/sample.html`
- `localhost:3000/countries` returns a list of countries which is fetched from the database